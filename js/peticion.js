function cargarAjax() {
    const url = "alumnos.json";

    axios
    .get(url)
    .then((res)=>{
        mostrar(res.data)
    })
    .catch((err)=> {
        console.log("Surgió un error");
    })
    function mostrar(data) {
        const res = document.getElementById('respuesta');
        let prom = document.getElementById('promedioGeneral');
        res.innerHTML = "";

        let promedio = 0, promedioGeneral = 0;

        for(item of data) {
            promedio += (item.matematicas + item.quimica + item.fisica + item.geografia)/4;
            res.innerHTML += "<input class='campos' type='text' placeholder = "+item.id+" readonly disabled='true'> "+ 
            "<input class='campos' type='text' placeholder = "+item.matricula+" readonly disabled='true'>"+
            "<input class='campos' type='text' placeholder = "+item.nombre+" readonly disabled='true'>"+
            "<input class='campos' type='text' placeholder = "+item.matematicas+" readonly disabled='true'>"+
            "<input class='campos' type='text' placeholder = "+item.quimica+" readonly disabled='true'>"+
            "<input class='campos' type='text' placeholder = "+item.fisica+" readonly disabled='true'>"+
            "<input class='campos' type='text' placeholder = "+item.geografia+" readonly disabled='true'>"+
            "<input class='campos' type='text' placeholder = "+promedio+" readonly disabled='true'>"+"<br>";

            promedioGeneral+=promedio;
            promedio = 0;
        }

        promedioGeneral = promedioGeneral / 18;
        prom.innerHTML = "<p>Promedio General: </p><input class='campos' type='text' placeholder = "+promedioGeneral+" readonly disabled='true'>";

    }
}

const buscar = document.getElementById('btnMostrar');
buscar.addEventListener('click', function() {
    cargarAjax();
});

const limpiar = document.getElementById('btnLimpiar');
limpiar.addEventListener('click', function() {
    const res = document.getElementById('respuesta');
    const prom = document.getElementById('promedioGeneral');

    res.innerHTML = "";
    prom.innerHTML = "";
});